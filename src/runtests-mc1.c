// -*- mode: mr32asm; tab-width: 2; indent-tabs-mode: nil; -*-
//----------------------------------------------------------------------------
// Copyright (c) 2023 Marcus Geelnard
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented// you must not
//     claim that you wrote the original software. If you use this software in
//     a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//----------------------------------------------------------------------------

#include "selftest.h"

#include <mc1/keyboard.h>
#include <mc1/leds.h>

static int s_pass;
static int s_fail;

static void test_result_handler(int pass, int test_no) {
  (void)test_no;
  if (pass) {
    ++s_pass;
  } else {
    ++s_fail;
  }
  sevseg_print_dec(s_pass);
}

int main(void) {
  // Run the tests.
  int result = selftest_run(&test_result_handler);

  // Display the result.
  if (result) {
    sevseg_print("PASS");
    set_leds((unsigned)s_pass);
  } else {
    sevseg_print("FAIL");
    set_leds((unsigned)s_fail);
  }

  // Wait for keyboard press.
  int terminate = 0;
  kb_init();
  while (!terminate) {
    kb_poll();
    unsigned event;
    while ((event = kb_get_next_event()) != 0U) {
      if (kb_event_is_press(event) && (kb_event_scancode(event) == KB_ESC)) {
        terminate = 1;
      }
    }
  }

  return result ? 0 : 1;
}

